document.getElementById('translateButton').addEventListener('click', function() {
  var inputText = document.getElementById('inputText').value;
  var translatedText = translateToRovarspraket(inputText);
  document.getElementById('translatedText').textContent = translatedText;
});

function translateToRovarspraket(text) {
  var translatedText = '';
  for (var i = 0; i < text.length; i++) {
    var char = text[i];
    if ('aeiouåäö'.includes(char.toLowerCase())) {
      translatedText += char;
    } else if ('bcdfghjklmnpqrstvwxz'.includes(char.toLowerCase())) {
      translatedText += char + 'o' + char;
    } else {
      translatedText += char;
    }
  }
  return translatedText;
}
